package hw2;

public class MathUtilities {
	
	public static double getHypotenuseDifference(double sideOne, double sideTwo){
		double hypotonous = Math.sqrt(Math.pow(sideOne, 2)+Math.pow(sideTwo, 2));
		return Math.abs(hypotonous-(sideOne+sideTwo));
	}
	public static double lerp(double x1, double y1, double x2, double y2, double x3 ){
		double slope=(y2-y1)/(x2-x1);
		return slope*(x3-x1)+y1;
	}
	
}
