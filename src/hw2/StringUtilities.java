package hw2;

import java.util.Scanner;

public class StringUtilities {
	
	public static String extractBracketed(String str){
		int leftBracket = str.indexOf("[");
		int rightBracket = str.indexOf("]");
		return str.substring(leftBracket+1, rightBracket);
	}
	public static String getThird(String word){
		Scanner input = new Scanner(word);
		input.useDelimiter(",");
		String notUsed1 = input.next();
		String notUsed2 = input.next();
		String used = input.next();
		return used;
		
	}
}
