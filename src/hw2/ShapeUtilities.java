package hw2;

public class ShapeUtilities {

	public static double getCircleArea(double radius){
		return Math.PI*radius*radius;
	}
	public static double getSquareWaste(double length){
		return ((length*length)-getCircleArea(length/2));
	}
	public static double getAnnulusArea(double iRadious, double oRadious){
		return Math.abs(getCircleArea(oRadious)-getCircleArea(iRadious));
	}

}
