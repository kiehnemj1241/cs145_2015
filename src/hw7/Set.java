package hw7;

import java.util.ArrayList;

public class Set {
	
	private ArrayList<String> list = new ArrayList<>();
	
	public Set(){
		
	}
	public boolean has(String item){
		return list.contains(item);
	}
	
	public void add(String item){
		if (!(list.contains(item))){
			list.add(item);
		}
	}
	
	public String toString(){
		String returnString = "";
		for(int i = 0; i < list.size(); i++){
			returnString += list.get(i);
			if (i != list.size() - 1){
				returnString += System.lineSeparator();
			}
		}
		return returnString;
	}
}
