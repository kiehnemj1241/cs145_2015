package hw7;

import java.io.IOException;
import java.util.ArrayList;

public class PlacesCache {

	private ArrayList<Place> places = new ArrayList<>();

	public PlacesCache() {
		places.clear();
	}

	public boolean isCached(String name) {
		for (int i = 0; i < places.size(); i++) {
			if (places.get(i).getName().equals(name)) {
				return true;
			}
		}
		return false;
	}
	public Place getPlace(String place) throws IOException, NoSuchPlaceException{
		for (int i = 0; i < places.size(); i++) {
			if (places.get(i).getName().equals(place)) {
				return places.get(i);
			}
		}
		Place holder = new Place(place);
		places.add(holder);
		return holder;
	}
	public int size(){
		return places.size();
	}
	public Place get(int index){
		return places.get(index);
	}
	
}
