package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class DondeUtilities {

	public static PlacesCache readCSV(File file) throws IOException, FileNotFoundException {
		PlacesCache thing = new PlacesCache();
		Scanner s = new Scanner(file);
		while (s.hasNextLine()) {
			String[] holder = s.nextLine().split("\\|", -1);
			try {
				thing.getPlace(holder[1]).addPerson(holder[0]);
			} catch (NoSuchPlaceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return thing;
	}

	public static void writeKML(PlacesCache list, File file) throws java.io.FileNotFoundException {
		PrintStream out = new PrintStream(file);
		out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + System.lineSeparator()
				+ "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" + System.lineSeparator() + "<Document>"
				+ System.lineSeparator() + "<name>Donde</name>");
		for (int i = 0; i < list.size(); i++) {
			out.println(list.get(i).toKML());
		}
	    out.println("</Document>" + System.lineSeparator() + "</kml>");
	}
}
