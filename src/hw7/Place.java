package hw7;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

public class Place {

	private String name;
	private double latitude;
	private double longitude;
	private Set person;

	public String getName() {
		return name;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public Place(String name, double latitude, double longitude) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		person = new Set();
	}

	public URL toGeocodeURL() throws URISyntaxException, MalformedURLException {

		URI help = new URI("http", "www.twodee.org", "/teaching/cs145/2015c/homework/hw7/geocode.php", "place=" + name,
				null);
		return help.toURL();
	}

	public Place(String place) throws IOException, NoSuchPlaceException {

		name = place;
		try {
			URL holder = toGeocodeURL();
			String string = hw7.WebUtilities.slurpURL(holder);
			Scanner s = new Scanner(string);
			if (s.hasNextDouble()) {
				this.latitude = s.nextDouble();
				this.longitude = s.nextDouble();
			} else {
				throw new NoSuchPlaceException("I really do not enjoy this...");
			}
		} catch (MalformedURLException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		person = new Set();
	}

	public void addPerson(String person) {
		this.person.add(person);
	}

	public String toKML() {
		String holder = "<Placemark>" + System.lineSeparator() + "<name>" + name + "</name>" + System.lineSeparator()
				+ "<description>" + person + "</description>" + System.lineSeparator() + "<Point><coordinates>"
				+ String.format("%.2f", this.longitude) + "," + String.format("%.2f", this.latitude)
				+ "</coordinates></Point>" + System.lineSeparator() + "</Placemark>";
		return holder;
	}

}
