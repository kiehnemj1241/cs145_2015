package hw5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PlayrightViewer extends JFrame implements ChangeListener {
  private BufferedImage image;
  private int windowWidth;
  private int windowOffset;
  private PlayrightCanvas panel;
  private JSlider slider;
  private JCheckBox box;

  public PlayrightViewer(BufferedImage image,
                         int nFrames) {
    this.image = image;
    this.windowWidth = nFrames;

    JPanel toolbar = new JPanel(new BorderLayout());

    // A checkbox enables the filter.
    box = new JCheckBox();
    box.setSelected(true);
    box.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        slider.setEnabled(box.isSelected());
        panel.repaint();
      }
    });
    toolbar.add(box, BorderLayout.WEST);

    // A slider moves the filter left and right.
    slider = new JSlider(0, 20 * (nFrames - 1), 0);
    slider.addChangeListener(this);
    toolbar.add(slider);

    add(toolbar, BorderLayout.NORTH);

    // Drawing the blinders requires some custom drawing, so we create a special
    // canvas.
    panel = new PlayrightCanvas();
    panel.setMinimumSize(new Dimension(image.getWidth(), image.getHeight()));
    panel.setPreferredSize(panel.getMinimumSize());
    panel.setMaximumSize(panel.getMinimumSize());
    add(panel);

    pack();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  private class PlayrightCanvas extends JPanel {
    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.drawImage(image, 0, 0, null);

      if (box.isSelected()) {
        g.setColor(Color.BLACK);

        // Draw all the full blinds.
        int startOfFirst = (windowOffset + 1) % windowWidth;
        int endOfCurrent = startOfFirst + windowWidth - 2;
        for (int c = startOfFirst; endOfCurrent < image.getWidth(); c += windowWidth) {
          g.fillRect(c, 0, windowWidth - 1, image.getHeight());
          endOfCurrent += windowWidth;
        }

        // Put a partial rectangle on the left. This is only needed if our
        // current offset "splits" the blinds within the window.
        if (windowOffset > 0 && windowOffset < windowWidth - 1) {
          g.fillRect(0, 0, windowOffset, image.getHeight());
        }

        // Put a partial rectangle on the right. This is only needed if the
        // last blind gets cut off by the image's right bound. I wrote this
        // stuff, but forging the expression didn't employ any intuition.
        int startOfLast = image.getWidth() - (image.getWidth() + windowWidth - 1 - windowOffset) % windowWidth;
        int endOfLast = startOfLast + windowWidth - 2;
        if (startOfLast < image.getWidth() && endOfLast >= image.getWidth()) {
          g.fillRect(startOfLast, 0, image.getWidth() - startOfLast, image.getHeight());
        }
      }
    }
  }

  @Override
  public void stateChanged(ChangeEvent event) {
    windowOffset = slider.getValue() % windowWidth;
    panel.repaint();
  }
}