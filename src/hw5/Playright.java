package hw5;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class Playright {

	public static File[] getSortedContents(File file) {
		File[] groupFile = file.listFiles();
		Arrays.sort(groupFile);
		return groupFile;
	}

	public static File[] filterMeta(File[] file) {
		int timeImage = 0;
		File[] tempArrayFile = new File[file.length];
		for (int x = 0; x < file.length; ++x) {
			String tempFile = file[x].getName();
			if (!tempFile.equals(".DS_Store") && !tempFile.equals("Thumbs.db")) {
				tempArrayFile[timeImage] = file[x];
				timeImage++;
			}
		}
		File[] finalFiles = Arrays.copyOf(tempArrayFile, timeImage);
		return finalFiles;

	}

	public static BufferedImage[] readImages(File[] file) throws IOException {
		BufferedImage[] imageArray = new BufferedImage[file.length];
		for (int x = 0; x < file.length; ++x) {
			imageArray[x] = ImageIO.read(file[x]);
		}
		return imageArray;
	}

	public static BufferedImage compress(BufferedImage[] imageArray) throws IllegalArgumentException {
		if (imageArray.length == 0) {
			throw new IllegalArgumentException("I once was a young lad....");
		}
		BufferedImage finalImage = new BufferedImage(imageArray[0].getWidth(), imageArray[0].getHeight(),imageArray[0].getType());
		for (int x = 0; x < finalImage.getWidth(); ++x) {
			for (int y = 0; y < finalImage.getHeight(); ++y) {
				finalImage.setRGB(x, y, imageArray[x % imageArray.length].getRGB(x, y));
			}
		}
		return finalImage;
	}

	public static void compressAndShow(File file) throws IOException {
		File[] stepOne = getSortedContents(file);
		File[] stepTwo = filterMeta(stepOne);
		BufferedImage[] stepThree = readImages(stepTwo);
		BufferedImage stepFinal = compress(stepThree);
		new hw5.PlayrightViewer(stepFinal, stepTwo.length);
	}
}
