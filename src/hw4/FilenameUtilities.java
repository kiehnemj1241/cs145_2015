package hw4;

import java.io.File;

public class FilenameUtilities {
	public static String getExtension(File file) {
		String newFile = file.getName();
		int periodPos = newFile.lastIndexOf('.');
		if ((periodPos < newFile.length()) && (periodPos > 0)) {
			return newFile.substring(periodPos + 1, newFile.length());
		} else {
			return null;
		}
	}

	public static File appendToName(File file, String extention) {
		String newFile = file.getPath();
		if (getExtension(file) != null){
			int periodPos = newFile.lastIndexOf('.');
			return new File(newFile.substring(0, periodPos) + "_" + 
			extention + newFile.substring(periodPos, newFile.length()));
		} else {
			return new File(newFile + "_" + extention);
		}
	}

}
