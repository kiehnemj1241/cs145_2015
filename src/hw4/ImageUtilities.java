package hw4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import java.awt.Color;

public class ImageUtilities {

	public static void main(String[] args) {
		/*
		 * BufferedImage dst = getCircleMask(50,50,2); try { ImageIO.write(dst,
		 * "png", new File("C:\\Users\\Matthew\\Desktop\\test.png")); } catch
		 * (IOException e) { e.printStackTrace(); }
		 */
	}

	public static BufferedImage swapCorners(BufferedImage image) {
		BufferedImage end = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		int xHalf = (image.getWidth() - image.getWidth() / 2);
		int yHalf = (image.getHeight() - image.getHeight() / 2);

		for (int y = 0; y < image.getHeight(); ++y) {
			for (int x = 0; x < image.getWidth(); ++x) {

				int finalx = (x + xHalf) % image.getWidth();
				int finaly = (y + yHalf) % image.getHeight();
				end.setRGB(finalx, finaly, image.getRGB(x, y));
			}
		}
		return end;
	}

	public static BufferedImage getCircleMask(int width, int height, double power) {
		BufferedImage end = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		// 1
		double xCenter = width / 2.0;
		// 2
		double yCenter = height / 2.0;
		// 4
		double radius = Math.min(xCenter, yCenter);
		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				// 3
				double distance = Math.hypot((x - xCenter), (y - yCenter));
				// 5
				double normalizedDistance = distance / radius;
				// 6
				double tweekDistance = Math.pow(normalizedDistance, power);
				// 7
				float gray = 1 - (float) tweekDistance;
				if (distance > radius) {
					end.setRGB(x, y, Color.BLACK.getRGB());
				} else {
					Color temp = new Color(gray, gray, gray);
					end.setRGB(x, y, temp.getRGB());
				}
			}
		}
		return end;
	}

	public static int mix(int a, int b, double mixxer) {
		return (int) ((mixxer * a) + ((1 - mixxer) * b));
	}

	public static Color mix(Color colorOne, Color colorTwo, double mixxer) {
		int redA = colorOne.getRed();
		int greenA = colorOne.getGreen();
		int blueA = colorOne.getBlue();
		
		int redB = colorTwo.getRed();
		int greenB = colorTwo.getGreen();
		int blueB = colorTwo.getBlue();
		
		int redMix = mix(redA, redB, mixxer);
		int greenMix = mix(greenA, greenB, mixxer);
		int blueMix = mix(blueA, blueB, mixxer);
		return new Color(redMix,greenMix,blueMix);
	}

	public static BufferedImage addMasked(BufferedImage a, BufferedImage am, BufferedImage b, BufferedImage bm) {
		BufferedImage end = new BufferedImage(a.getWidth(), a.getHeight(), a.getType());
		for (int x = 0; x < a.getWidth(); x++) {
			for (int y = 0; y < a.getHeight(); y++) {
				// 10
				int rgba = am.getRGB(x, y);
				Color ATempRGB = new Color(rgba);
				// 11
				int rgbb = bm.getRGB(x, y);
				Color BTempRGB = new Color(rgbb);
				// 12
				double aWeight = ATempRGB.getRed();
				double bWeight = BTempRGB.getRed();
				double totalWeight = aWeight + bWeight;
				// 13
				double weight;
				if (totalWeight == 0) {
					weight = .5;
				} else {
					weight = aWeight / totalWeight;
				}
				// 14
				Color colorA = new Color(a.getRGB(x, y));
				Color colorB = new Color(b.getRGB(x, y));
				Color colorBlend = mix(colorA,colorB,weight);
				end.setRGB(x, y, colorBlend.getRGB());
			}
		}
		return end;
	}

	public static BufferedImage makeSeamless(BufferedImage image, double power) {
		// a
		BufferedImage imageB = swapCorners(image);
		// b
		BufferedImage maskA = getCircleMask(image.getWidth(), image.getHeight(), power);
		// c
		BufferedImage maskB = swapCorners(maskA);
		// d
		BufferedImage end = addMasked(image, maskA, imageB, maskB);
		return end;
	}

	public static BufferedImage tile(BufferedImage image, int width, int height) {
		BufferedImage end = new BufferedImage(image.getWidth() * width, image.getHeight() * height, image.getType());
		for (int x = 0; x < end.getWidth(); ++x) {
			for (int y = 0; y < end.getHeight(); ++y) {
				Color temp = new Color(image.getRGB(x % image.getWidth(), y % image.getHeight()));
				end.setRGB(x, y, temp.getRGB());
			}
		}
		return end;
	}
}