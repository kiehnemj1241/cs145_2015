package hw3;

public class Trutilities {
	
	public static boolean inBox(double llx, double lly, 
								double urx, double ury, 
								double rx, double ry){
		return (rx<= urx && rx>= llx)&&(ry<= ury && ry>= lly);
	}
	public static boolean inCircle(double cx, double cy, double r, double rx, double ry){
		return r>= Math.hypot(Math.abs(cx-rx),Math.abs(cy-ry) );
	}
	public static boolean isGrayscale(String rgb){
		String r = rgb.substring(1, 3);
		String g = rgb.substring(3, 5);
		String b = rgb.substring(5, 7);
		return r.equals(g)&&g.equals(b);
	}
	public static boolean isNorth(double degrees,String dms){
		int degreePos = dms.indexOf('\u00B0');
		int semiPos = dms.indexOf('\'');
		int quotePos = dms.indexOf('\"');
		double otherDegrees = Integer.valueOf(dms.substring(0, degreePos));
		double minutes = Integer.valueOf(dms.substring(degreePos+1, semiPos));
		double seconds = Integer.valueOf(dms.substring(semiPos+1, quotePos));
		return degrees > (otherDegrees+(minutes/60)+(seconds/3600));
	}
	public static boolean isBefore(int y1, int m1, int d1, int y2, int m2, int d2){
		boolean check1 = (y1<y2);
		boolean check2 = (y1<=y2)&&(m1<m2);
		boolean check3 = (y1<=y2)&&(m1<=m2)&&(d1<d2);
		return check1||check2||check3;
	}
	public static boolean isEqualEnough( double a, double b, double threshold){
		return (Math.abs(a-b)<Math.abs(threshold));
	}
	public static boolean isSumTriplet(int a,int b, int c){
		return (a+b==c)||(b+c==a)||(c+a==b);
	}
	public static boolean isGameOver(String str){
		str=str.toLowerCase();
		return str.equals("quit")||str.equals("exit")||str.equals("done");
	}
}
