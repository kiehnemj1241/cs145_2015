package hw6;

import java.util.ArrayList;
import java.util.Arrays;

public class PlotPlot {

	public static int[] getChoicesForOnePage(String p) {
		if (p.equals("p")) {
			return null;
		} else if (p.length() == 0) {
			return new int[0];
		} else {
			String[] tempStringArray = p.split(",");
			int[] returnArray = new int[tempStringArray.length];
			for (int i = 0; i < tempStringArray.length; ++i) {
				returnArray[i] = Integer.parseInt(tempStringArray[i]) - 1;
			}
			return returnArray;
		}
	}

	public static int[][] getChoicesForAllPages(String input) {
		String[] testing = input.split("\\|", -1);
		int[][] q = new int[testing.length][];
		for (int i = 0; i < testing.length; i++) {
			q[i] = getChoicesForOnePage(testing[i]);
		}
		return q;
	}

	public static ArrayList<Integer> getForkPages(int[][] pages) {

		ArrayList<Integer> optionPages = new ArrayList<Integer>();

		for (int i = 0; i < pages.length; ++i) {
			if (pages[i] != null && pages[i].length > 1) {
				optionPages.add(i);
			}
		}
		return optionPages;
	}

	public static ArrayList<Integer> getEndPages(int[][] pages) {

		ArrayList<Integer> endPages = new ArrayList<Integer>();
		for (int i = 0; i < pages.length; ++i) {
			if (pages[i] != null && pages[i].length == 0) {
				endPages.add(i);
			}
		}
		return endPages;
	}

	public static ArrayList<Integer> getOrphanPages(int[][] pages) {

		ArrayList<Integer> hasPrevious = new ArrayList<>();

		hasPrevious.add(0);
		for (int p = 0; p < pages.length; p++) {
			if (pages[p] == null) {
				if (!hasPrevious.contains(p)) {
					hasPrevious.add(p);
				}
			} else {
				for (int c = 0; c < pages[p].length; c++) {
					if (!hasPrevious.contains(pages[p][c])) {
						hasPrevious.add(pages[p][c]);
					}
				}
			}
		}

		ArrayList<Integer> returnArray = new ArrayList<>();
		for (int i = 0; i < pages.length; i++) {
			if (!hasPrevious.contains(i)) {
				returnArray.add(i);
			}
		}
		return returnArray;
	}

	public static void dotEndPages(int[][] pages) {
		for (int p = 0; p < pages.length; p++) {
			if (pages[p] != null && pages[p].length == 0) {
				System.out.println("  " + (p + 1) + " [peripheries=2];");
			}
		}
	}

	public static void dotOrphanPages(int[][] pages) {

		ArrayList<Integer> helo = getOrphanPages(pages);
		for (int i = 0; i < helo.size(); i++) {
			System.out.println("  " + (helo.get(i) + 1) + " [style=dashed];");
		}
	}

	public static void dotPlot(int[][] pages) {
		for (int i = 0; i < pages.length; i++) {
			if (pages[i] != null) {
				for (int c = 0; c < pages[i].length; c++) {
					if (pages[i].length > 0) {
						System.out.println("  " + (i + 1) + " -> " + (pages[i][c] + 1) + ";");
					}
				}
			}
		}
	}

	public static void dot(int[][] pages) {
		System.out.println("digraph G {");
		dotEndPages(pages);
		dotOrphanPages(pages);
		dotPlot(pages);
		System.out.println("}");
	}

	public static ArrayList<Integer> getPathOfFirsts(int[][] pages) {
		ArrayList<Integer> hello = new ArrayList<>();
		int pos = 0;
		while (pages[pos] != null && pages[pos].length != 0) {
			hello.add(pos);
			pos = pages[pos][0];
		}
		hello.add(pos);
		return hello;
	}
}
